# OpenML dataset: dresses-sales

https://www.openml.org/d/45061

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contain attributes of dresses and their recommendations according to their sales. Sales are monitor on the basis of alternate days.The attributes present analyzed are: Recommendation, Style, Price, Rating, Size, Season, NeckLine, SleeveLength, waiseline, Material, FabricType, Decoration, Pattern, Type. In this dataset they are named Class(target) and then subsequently V2 - V13.Contact:Muhammad Usman & Adeel Ahmed, usman.madspot '@' gmail.com adeel.ahmed92 '@' gmail.com, Air University, Students at Air University.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45061) of an [OpenML dataset](https://www.openml.org/d/45061). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45061/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45061/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45061/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

